const sections = document.querySelectorAll('section')

sections.forEach(section => {
    section.addEventListener('click', waves);
});

function waves(e) {
    const d = Math.max(this.clientWidth, this.clientHeight);
    const waves = document.createElement('div');
    waves.classList.add('waves');
    this.appendChild(waves);
    waves.style.width = waves.style.height = d + 'px';
    waves.style.left = e.pageX - this.offsetLeft - d / 2 + 'px'
    waves.style.top = e.pageY - this.offsetTop - d / 2 + 'px'
    setTimeout(() => { 
        this.removeChild(waves)
    },600)
}